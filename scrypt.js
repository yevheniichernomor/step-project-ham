const tabServiceMenuItem = document.querySelectorAll('.s-menu-item');
const tabServiceContent = document.querySelectorAll('.s-menu-text');
const tabServiceImage = document.querySelectorAll('.s-menu-image');
const tabTriangle = document.querySelectorAll('.t-box');

function tabs() {
    for(let i = 0; i < tabServiceMenuItem.length; i++){
        tabServiceMenuItem[i].addEventListener('click', (event) => {
            
            let tabCurrent = event.target.parentElement.children;
            for(let j = 0; j < tabCurrent.length; j++){
                tabCurrent[j].classList.remove('active-s-menu-item')
            }
            event.target.classList.add('active-s-menu-item');
    
            let contentCurrent = event.target.parentElement.parentElement.children[3].children;
            for(let c = 0; c < contentCurrent.length; c++){
                contentCurrent[c].classList.remove('text-active');
            }
    
            let imageCurrent = event.target.parentElement.parentElement.children[3].children[0].children;
            for(let d = 0; d < imageCurrent.length; d++){
                imageCurrent[d].classList.remove('text-active');
            }

            let triangleCurrent = event.target.parentElement.nextElementSibling.children;
            for (let t = 0; t < triangleCurrent.length; t++){
                triangleCurrent[t].classList.remove('t-box-active');
            }
    
            tabServiceImage[i].classList.add('text-active');
            tabServiceContent[i].classList.add('text-active');
            tabTriangle[i].classList.add('t-box-active');       
            
        });
    }

}

tabs();

const list = document.querySelector('.list');
const items = document.querySelectorAll('.blocks-item');
const listItems = document.querySelectorAll('.list-item')

function filter(){
    list.addEventListener('click', event => {
        const targetId = event.target.dataset.id;
        const target = event.target; 
        if(target.classList.contains('list-item')){
            listItems.forEach(listItem => listItem.classList.remove('list-item-active'));
            target.classList.add('list-item-active'); 
        } 

        switch(targetId){
            case 'all':
                getItems('blocks-item')
                break
                case 'graphic-design':
                    getItems(targetId)
                    break
                case 'web-design':
                    getItems(targetId)
                    break
                case 'wordpress':
                    getItems(targetId)
                    break
                case 'landing-pages':
                    getItems(targetId)
                    break
            }
    })
}

filter();

function getItems(className) {
    items.forEach(item => {
        if (item.classList.contains(className) && !item.classList.contains('inactive')) {
            item.style.display = 'inline-block'
        } 
        else if(!item.classList.contains('inactive')) {
            item.style.display = 'none'
        }
        
    })
    
}

const loadBtn = document.getElementById('load-more-btn');
const sectionThree = document.querySelector('.section-three');
const blocksInactive = document.getElementById('inactive');

function loadMore() {
    sectionThree.style.height = 1800 + 'px';    
    loadBtn.style.display = 'none';
    items.forEach(item => {
        item.classList.remove('inactive')
    })
    
}

loadBtn.addEventListener('click', loadMore);

const largePhotoSliderLine = document.querySelector('.large-photo-slider-line');
const reviewSlider = document.querySelector('.review-slider-line');
const nameSlider = document.querySelector('.name-slider-line');
const prevButton = document.querySelector('.prev');
const nextButton = document.querySelector('.next');
const photos = document.querySelectorAll('.photo');
const userBoxes = document.querySelectorAll('.user-box');
const reviews = document.querySelectorAll('.review');
const largePhotos = document.querySelectorAll('.large-photo');

let position = 0;
let photoIndex = 0;
let reviewPosition = 0;
let namePosition = 0;


const nextSlide = () => {
    if(position < 498 && reviewPosition < 180 && namePosition < 180){
        position += 166;
        photoIndex++;
        reviewPosition += 60;
        namePosition += 60
    } else {
        position = 0;
        photoIndex = 0;
        reviewPosition = 0;
        namePosition = 0;
    }    
    
    largePhotoSliderLine.style.left = -position + 'px';
    reviewSlider.style.top = -reviewPosition + 'px';
    nameSlider.style.top = -namePosition + 'px';
    thisPhoto(photoIndex);
    thisBox(photoIndex);
    thisReview(photoIndex);
    thisLargePhoto(photoIndex);
}

const prevSlide = () => {
    if(position > 0 && reviewPosition > 0 && namePosition > 0){
        position -= 166;
        photoIndex--;
        reviewPosition -= 60;
        namePosition -= 60
    } else {
        position = 498;
        photoIndex = 3;
        reviewPosition = 180;
        namePosition = 180
    }
    
    largePhotoSliderLine.style.left = -position + 'px';
    reviewSlider.style.top = -reviewPosition + 'px';
    nameSlider.style.top = -namePosition + 'px';
    thisPhoto(photoIndex);
    thisBox(photoIndex);
    thisReview(photoIndex);
    thisLargePhoto(photoIndex);
}

nextButton.addEventListener('click', nextSlide);
prevButton.addEventListener('click', prevSlide);

const thisPhoto = (index) => {
    for (let photo of photos) {
        photo.classList.remove('active-photo');
    }
    photos[index].classList.add('active-photo');
}

const thisBox = (index) => {
    for (let userBox of userBoxes) {
        userBox.classList.remove('lp-active');
    }
    userBoxes[index].classList.add('lp-active');
}

const thisReview = (index) => {
    for (let review of reviews) {
        review.classList.remove('lp-active');
    }
    reviews[index].classList.add('lp-active');
}

const thisLargePhoto = (index) => {
    for (let largePhoto of largePhotos) {
        largePhoto.classList.remove('lp-active');
    }
    largePhotos[index].classList.add('lp-active');
}

photos.forEach((photo, index) => {
    photo.addEventListener('click', () => {
        position = 166 * index;
        largePhotoSliderLine.style.left = -position + 'px';
        reviewPosition = 60 * index;
        reviewSlider.style.top = -reviewPosition + 'px';
        namePosition = 60 * index;
        nameSlider.style.top = -reviewPosition + 'px';
        photoIndex = index;
        thisPhoto(photoIndex);
        thisBox(photoIndex);
        thisReview(photoIndex);
        thisLargePhoto(photoIndex);
    })
})



